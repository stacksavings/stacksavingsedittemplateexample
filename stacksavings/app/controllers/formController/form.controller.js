'use strict';

serviceJoinApp.controller('formCtrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
    $scope.stacksavingsForm = {};
        $scope.saveForm = function(file, form_name){
            var rString = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            var profileId = "profile" + rString
            imageUplaod.getSignedUrl(file.type, profileId).then(function(result){
              var src = result.data.resultUrl + '?v=' + Math.random();
              $scope.form.bgImage = src;
              $scope.form.formname = form_name;
              $scope.stacksavingsForm.data = {}
              var stacksavingsFormData = $scope.stacksavingsForm.data
              stacksavingsFormData.firstname = $scope.form.firstname
              stacksavingsFormData.lastname = $scope.form.lastname
              stacksavingsFormData.phone = $scope.form.phone
              stacksavingsFormData.email = $scope.form.email
              stacksavingsFormData.message = $scope.form.message
              stacksavingsFormData.bgImage = $scope.form.bgImage
              stacksavingsFormData.formname = $scope.form.formname

              angular.copy($scope.form);
              formService.saveForm($scope.stacksavingsForm).then(function(result){
                  debugger;
              })
                Upload.http({
                    method : "PUT",
                    url    : result.data.oneTimeUploadUrl,
                    headers: {
                            'Content-Type': file.type != '' ? file.type : 'application/octet-stream'
                        },
                    data   : file
                    }).then(function (resp) {
                        //$('.post-content-text').css('background-image', 'url(\'' + src + '\')');
                        //$('#thumbnail-img').hide();
                    },function (response) {
                        if (response.status > 0)
                        self.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        //$scope.picFile.progress = file.progress;
                });
            });
        }
    }]);

    serviceJoinApp.controller('form1Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
        $scope.stacksavingsForm = {};
        $scope.saveForm1 = function(){
          $scope.form1.formname = "webform";
          $scope.stacksavingsForm.data = angular.copy($scope.form1);
            formService.saveForm($scope.stacksavingsForm).then(function(result){
                debugger;
            })
        }
    }]);

    serviceJoinApp.controller('form2Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
      $scope.stacksavingsForm = {};
      $scope.saveForm2 = function(){
        $scope.form2.formname = "webform";
        $scope.stacksavingsForm.data = angular.copy($scope.form2);
          formService.saveForm($scope.stacksavingsForm).then(function(result){
              debugger;
          })
      }
    }]);


    serviceJoinApp.controller('form3Ctrl', ['$scope','imageUplaod', 'Upload', 'formService', function ($scope, imageUplaod, Upload, formService) {
      $scope.stacksavingsForm = {};
      $scope.saveForm3 = function(){
        $scope.form3.formname = "webform";
        $scope.stacksavingsForm.data = angular.copy($scope.form3);
          formService.saveForm($scope.stacksavingsForm).then(function(result){
              debugger;
          })
      }
    }]);


function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
