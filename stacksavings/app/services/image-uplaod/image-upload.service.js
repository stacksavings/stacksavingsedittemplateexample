'use strict';

serviceJoinApp.service('imageUplaod', ['$q', '$resource', 'apiBaseUrl', '$filter', '$rootScope', '$http', function ($q, $resource, apiBaseUrl, $filter, $rootScope, $http) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var api = $resource(apiBaseUrl + 'updateFull');
        return {
            getSignedUrl: function (file_type, img_name) {
                    var data = {
                        tpl        : 'blog-template',
                        contentType: file_type,
                        imgName    : img_name,
                        page       : 'blog-template_home',
                        auth_token : 'blog-template'
                    };
                    return $http.post(apiBaseUrl + 'getSignedUrl', data);
            }
        }
    }]);
